import { Inject, Injectable } from '@nestjs/common';
import { IBestBuyers, IBestSellers } from '../Model/IReports';
import { OrderService } from '../../Order/Service/OrderService';
import { orderBy, get } from 'lodash';

@Injectable()
export class ReportService {
    @Inject() orderService: OrderService;

    async collectBestSellersData(date: Date): Promise<IBestSellers[]> {
        const orders = await this.orderService.getOrdersByDay(date);

        const products: {[id: number]: {productName: string, quantity: number, totalPrice: number}} = {};
        orders.forEach(order => order.products.forEach(product => {
            products[product.id] = {
                productName: product.name,
                quantity: get(products, [ product.id, 'quantity' ], 0) + 1,
                totalPrice: get(products, [ product.id, 'totalPrice' ], 0) + product.price,
            };
        }));

        return orderBy(Object.values(products), [ 'quantity', 'productName' ], [ 'desc', 'asc' ]);
    }

    async collectBestBuyersData(date: Date): Promise<IBestBuyers[]> {
        const orders = await this.orderService.getOrdersByDay(date);

        const customers: {[id: number]: {customerName: string, totalPrice: number}} = {};
        orders.forEach(order => order.products.forEach(product => {
            customers[order.customer.id] = {
                customerName: order.customer.firstName,
                totalPrice: get(customers, [ order.customer.id, 'totalPrice' ], 0) + product.price,
            };
        }));

        return orderBy(Object.values(customers), [ 'totalPrice', 'customerName' ], [ 'desc', 'asc' ]);
    }
}
