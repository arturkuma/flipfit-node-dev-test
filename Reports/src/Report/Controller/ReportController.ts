import { Controller, Get, Inject, Param } from '@nestjs/common';
import { IBestBuyers, IBestSellers } from '../Model/IReports';
import { ReportService } from '../Service/ReportService';

@Controller()
export class ReportController {
    @Inject() reportService: ReportService;

    @Get('/report/products/:date')
    bestSellers(@Param('date') inputDate: string): Promise<IBestSellers[]> {
        const date = new Date(inputDate);
        return this.reportService.collectBestSellersData(date);
    }

    @Get('/report/customer/:date')
    bestBuyers(@Param('date') inputDate: string): Promise<IBestBuyers[]> {
        const date = new Date(inputDate);
        return this.reportService.collectBestBuyersData(date);
    }
}
