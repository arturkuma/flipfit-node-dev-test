import { Customer } from './Customer';
import { Product } from './Product';

export class Order {
    number: string;
    customer: Customer;
    createdAt: Date;
    products: Product[];
}
