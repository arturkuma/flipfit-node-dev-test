import { Module } from '@nestjs/common';
import { Repository } from './Service/Repository';
import { OrderMapper } from './Service/OrderMapper';
import { OrderService } from './Service/OrderService';

@Module({
    controllers: [],
    providers: [ Repository, OrderMapper, OrderService ],
    exports: [ OrderMapper, OrderService ],
})
export class OrderModule {
}
