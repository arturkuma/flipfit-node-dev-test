import { Inject, Injectable } from '@nestjs/common';
import { Repository } from './Repository';
import { OrderMapper } from './OrderMapper';
import { Order } from '../Model/Order';
import { datesAreSameDay } from '../../functions';

@Injectable()
export class OrderService {
    @Inject() repository: Repository;
    @Inject() orderMapper: OrderMapper;

    async fetchOrders(): Promise<Order[]> {
        return Promise.all((await this.repository.fetchOrders()).map(order => this.orderMapper.mapOrder(order)));
    }

    async getOrdersByDay(date: Date): Promise<Order[]> {
        return (await this.fetchOrders()).filter(order => datesAreSameDay(date, order.createdAt));
    }
}
