import { Injectable } from '@nestjs/common';
import { Customer } from '../Model/Customer';
import { Product } from '../Model/Product';

/**
 * Data layer - mocked
 */
@Injectable()
export class Repository {
    fetchOrders(): Promise<any[]> {
        return new Promise(resolve => resolve(require('../Resources/Data/orders')));
    }

    fetchProducts(): Promise<any[]> {
        return new Promise(resolve => resolve(require('../Resources/Data/products')));
    }

    fetchCustomers(): Promise<any[]> {
        return new Promise(resolve => resolve(require('../Resources/Data/customers')));
    }

    async fetchCustomer(id: number): Promise<Customer> {
        return (await this.fetchCustomers()).find(({ id: customerId }) => id === customerId);
    }

    async fetchProduct(id: number): Promise<Product> {
        return (await this.fetchProducts()).find(({ id: productId }) => id === productId);
    }
}
