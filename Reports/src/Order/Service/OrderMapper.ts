import { Injectable, Inject } from '@nestjs/common';
import { Repository } from './Repository';
import { Order } from '../Model/Order';

@Injectable()
export class OrderMapper {
    @Inject() repository: Repository;

    async mapOrder(object: { number: string, customer: number, createdAt: string, products: number[] }): Promise<Order> {
        return {
            ...(new Order()),
            ...object,
            customer: await this.repository.fetchCustomer(object.customer),
            createdAt: new Date(object.createdAt),
            products: await Promise.all(object.products.map(id => this.repository.fetchProduct(id))),
        };
    }
}
