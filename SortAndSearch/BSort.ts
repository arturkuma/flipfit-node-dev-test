// Insertion sort
export class BSort {
    static sort(elements: number[]): number[] {
        for (let i = 0; i < elements.length; i++) {
            let el = elements[i];
            let j = i - 1;

            for (j; j >= 0 && elements[j] > el; j--) {
                elements[j + 1] = elements[j];
            }

            elements[j + 1] = el;
        }

        return elements;
    }
}
