class BSearch {
    private _numberOfOperations = 0;

    get numberOfOperations(): number {
        return this._numberOfOperations;
    }

    search(elements: number[], value: any): number {
        this._numberOfOperations = 0;
        let [left, right] = [0, elements.length];

        while (left <= right) {
            this._numberOfOperations++;
            const mid = Math.floor((left + right) / 2);
            if (elements[mid] === value) {
                return mid
            } else if (elements[mid] < value) {
                left = mid + 1
            } else {
                right = mid - 1
            }
        }

        return -1;
    }
}

export default new BSearch();
