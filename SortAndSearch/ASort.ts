// Bubble sort
export class ASort {
    static sort(elements: number[]): number[] {
        for (let i = elements.length - 1; i >= 0; i--){
            for(let j = 1; j <= i; j++){
                if(elements[j - 1] > elements[j]){
                    const temporary = elements[j - 1];
                    elements[j - 1] = elements[j];
                    elements[j] = temporary;
                }
            }
        }

        return elements;
    }
}
