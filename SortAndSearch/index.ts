import { ASort } from './ASort';
import { BSort } from './BSort';
import BSearch from "./BSearch";

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [1, 5, 13, 27, 77];

console.log('Checking sorting algorithms');
console.log(ASort.sort(unsorted));
console.log(BSort.sort(unsorted));

[1, 5, 13, 27, 77, 666].forEach(targetValue => {
    console.log(
        `Looking for a value ${targetValue}, it's index is ${BSearch.search(elementsToFind, targetValue)}`,
        `Number of operations ${BSearch.numberOfOperations}`
    );
});
